      !this code generates an array of 100 random numbers
      program test_random_number
      implicit none
      real, dimension(100) :: r
      integer, dimension(100) :: r2
      integer :: m, ml


      
      call random_number(r)
      !print *, r

      r2 = 100*r
      print *, r2

      call findMax(r2, 100, m, ml)

      print *
      print *, m, ml
      print *, maxVal(r2), maxLoc(r2)

      contains

      subroutine findMax(a, a_size, maxValue, maxLocation)
      implicit none
      integer :: maxValue, maxLocation, i = 1
      integer :: a_size

      integer, dimension(a_size) :: a

      maxValue = a(1)
      maxLocation = 1

      do i = 1, a_size
         
         if (a(i) > maxValue) then
            maxValue = a(i)
            maxLocation = i
         end if

      end do

      end subroutine

      
      end program
