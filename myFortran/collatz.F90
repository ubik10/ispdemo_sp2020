
     program collatz
     implicit none

     integer :: u

     u = 10000

     do 
         print *, u

         if (u == 1) then
            exit
         end if

         if (mod(u,2) == 0) then
            u = u/2
         else
            u = 3*u + 1
         end if
     end do


     end program
