


     program dynalloc
     implicit none

     real, dimension(:, :), allocatable :: X

     integer(8) :: n, m
     integer :: ierror

     read *, n, m

     allocate(X(n, m), stat=ierror)
     if (ierror /= 0) then
        print *,"sorry, not enough memory"
        stop
     end if

     print *, "successful!"

     call arrayStats(X)

     contains

     subroutine arrayStats(myArray)

     real, dimension(:, :) :: myArray

     print *, shape(myArray)
     print *, size(myArray)


     end subroutine
     end program


