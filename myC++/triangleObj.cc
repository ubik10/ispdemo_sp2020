#include <iostream>
#include <vector>


using std::cout;
using std::endl;
using std::vector;

class Point
{
	private:
		float x;
		float y;
	public:
		Point()
		{
			x = 0; y = 0;
		}
		Point(float xin, float yin)
		{
			x = xin; y = yin;
		}
}

class Line
{
	private:
		Point p1;
		Point p2;
	public:
		Line()
		{
			p1 = Point();
			p2 = Point();
		}
		Line (Point pin1, Point pin2)
		{
			p1 = pin1;
			p2 = pin2;
		}
		Line (float x1, float y1, float x2, float y2)
		{
			p1 = Point(x1, y1);
			p2 = Point(x2, y2);
		}
}

