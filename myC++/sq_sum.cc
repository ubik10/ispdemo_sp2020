#include <iostream>

using std::cout;
using std::endl;


int sqsum(int i);

int main()
{
	cout << sqsum(100) << endl;
	return 0;
}

int sqsum(int i)
{
	if (i == 1)
	{
		return 1;
	}
	else
	{
		return i*i + sqsum(i-1);
	}
}
