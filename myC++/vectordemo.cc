#include <iostream>
#include <vector>

using std::vector;
using std::cout;
using std::endl;


int main()
{
	vector<float> x(10);
	vector<float> y{1, 2, 3, 4, 5};
    vector<float> d = {1, 2, 3, 4, 6};

	int i = 0;
    for (float &z : x)
	{
		z = i++;
	}

	for (float z : x)
	{
		cout << z << endl;
	}

	cout << x[5] << endl;
}


