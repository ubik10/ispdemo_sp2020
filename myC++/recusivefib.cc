#include <iostream>

using std::cout;
using std::endl;

int fib(int n);

int main()
{
	cout << fib(10) << endl;
	return 0;
}

int fib(int n)
{
	if (n == 0 || n == 1)
		return 1;
	else
		return fib(n-1) + fib(n-2);

}
