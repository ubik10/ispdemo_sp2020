#include <iostream>

using std::cout;
using std::endl;

int sum (int i);

int main()
{
	cout << sum(100) << endl;
	return 0;
}


int sum(int i)
{
	int cur;
	cout << "calculating " << i << endl;
	if (i == 0)
	{
	   cur = 0;
	}
	else
	{
		cur = i+sum(i-1);
		cout << "done " << i << endl;
	}

	return cur;
}


