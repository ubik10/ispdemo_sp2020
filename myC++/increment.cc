#include <iostream>


using std::cout;
using std::endl;

int main()
{
	
   int i = 0;
   i =i + 1; // increments the variable i by 1
   cout << “i is equal to “ << i << endl;
   cout << “i+1 is equal to “ << i << endl;
   cout << “i is equal to “ << i << endl;
   cout << “i++ is equal to “ << i++ << endl;
   cout << “i-- is equal to “ << i-- << endl;
   cout << “++i is equal to “ << ++i << endl;
   cout << “current value of i is equal to “ << i << endl;
   cout << “--i is equal to “ << --i << endl;
}
