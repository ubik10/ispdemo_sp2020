#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int fib(int i)
{
	int temp;

	if (i < 2)
		return 1;

	temp = fib(i-1) + fib(i-2);

	return temp;
}

int main()
{
	int start = 14;

	cout << fib(start) << endl;
}


