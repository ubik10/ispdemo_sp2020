#include <iostream>

using std::cout;
using std::endl;


int main()
{
	int i = 5;

	while (i < 10)
	{
		cout << i << endl;
		i++;
	}

}
