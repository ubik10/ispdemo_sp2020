#include <iostream>

using std::cout;
using std::endl;

struct node{
	int val;
	struct node *next;
	struct node *prev;
};

int main()
{
	node *head;
	node *cur;
    node *prev;
	node *tail;

	head = (node*)malloc (sizeof(node));

    head->val = 0;
	head->next = NULL;

    tail = head;
    prev = head;

	int i = 0;

	for (i = 0; i< 100; i++)
	{
		cur = (node*)malloc (sizeof(node));
                cur->val = i*2;
                cur->next = NULL;
 
		prev->next = cur;
        prev = cur;
		tail = cur;
    }
	tail->next = NULL;


    cur = head;
	
	while (cur != NULL)
	{
		cout << " ---- " << endl;
		cout << cur << endl;
		cout << cur->val << endl;
		cout << cur->next << endl;

		cur = cur->next;
	}

	return 0;

}

