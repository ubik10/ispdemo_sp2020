#include <iostream>

using std::cout;
using std::endl;

float f(float xOne, float xTwo);

int main()
{
	float local_y;
	local_y = f(1.5, 3.1415);
	cout << local_y << endl;

}

float f(float xOne, float xTwo)   /// xOne = 1.5    /// xTwo = 3.1415
{
	float x, x1, x2;
	x = xOne*xTwo;
	x1 = xOne+xTwo;
	x2 = xOne/xTwo;

	return x, x1, x2;
}


