#include <iostream>

using std::cout;
using std::endl;

float f(float &xOne, float &xTwo);

int main()
{
	float local_y;
	float local_x1 = 1.5;
	float local_x2 = 3.1415;

	local_y = f(local_x1, local_x2);

	cout << local_y << endl;
	cout << local_x1 << endl;
	cout << local_x2 << endl;

}

float f(float &xOne, float &xTwo)   /// &xOne = local_x1, &xTwo = local_x2
{
	float x, x1, x2;
	x1 = xOne; x2 = xTwo;

	x = x1*x2;
	xOne = x1 + x2;
	xTwo = x1/x2;

	return x;
}


