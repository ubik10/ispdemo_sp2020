#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int recurse_sum(int i);

int main()
{
	int sum = 0;

	for (int i = 0; i<101; i++)
	{
		sum = sum + i;
	}

	cout << sum << endl;

	int sum2 = 0;
	sum2 = recurse_sum(100);

	cout << sum2 << endl;

}

int recurse_sum(int i)
{
	if (i == 0) 
		return 0;
	else
		return i + recurse_sum(i-1);
}


