#include <iostream>

using std::cout;
using std::endl;


int main()
{
	float x[100];

	for (int i=0; i<100; i++)
	{
		x[i] = 2*i;
	}

	for (int i=0; i<10; i++)
	{
		cout << "x[" << i << "] " << &x[i] << endl;
	}


	cout << endl;
	return 0;
}
