#include <iostream>
#include <vector>
#include <cstdlib>


using std::vector;
using std::cout;
using std::endl;

class Person
{
	private:
		int designation;
		int age;
		vector <Person> interactions;
	
	public:
		Person()
		{
		}

		Person(int i)
		{
			age = i;
		}
		void setDesignation(int d)
		{
			designation = d;
		}

		int getDesignation()
		{
			return designation;
		}

		void setage(int newage)
		{
			age = newage;
		}

		int getage()
		{
			return age;
		}

		void addInteractionVector(vector <Person> p_interactions)
		{
			interactions = p_interactions;
		}

		void printInteractionVection()
		{
			for (auto p : interactions)
			{
				cout << p.designation << " ";
			}
		}

};

class Population
{
	private:
		vector<Person> P;
	
	public:
		Population()
		{
		}
        Population(int x, int n_interactions)
		{
			for (int i = 0; i < x; i++)
			{
				Person j;
				j.setDesignation(i);
				j.setage(rand()%100);
				P.push_back(j);
			}

			//fill interactions per person;
			

			for (int j = 0; j < x; j++)
			{
                vector<Person> p_interactions;
				for (int i = 0; i < n_interactions; i++)
				{
			      int r  = rand()%x;
				  for (auto p : p_interactions)
				  {
					  while (p.getDesignation() == r)
					  {
						  r = rand()%x;
					  }

				  }
				  p_interactions.push_back(P[r]);
				}

				P[j].addInteractionVector(p_interactions);
			}

			

		}

		void printPeople()
		{
			for (auto person : P)
			{
				cout << person.getDesignation() << "  "  << person.getage() << endl;
			}
		}

		vector<Person> getPeopleV()
		{
			return P;
		}
};



int main()
{
	Person joe;
	joe.setage(10);

    Population myPop(100, 10);

	myPop.printPeople();

    for (auto person : myPop.getPeopleV())
	{
		cout << person.getDesignation() << " interacted with: " << endl;
		person.printInteractionVection();
		cout << endl << endl;
	}

}
