#include <iostream>

using std::cout;
using std::endl;

using std::cin;
using std::string;

int main()
{
	string myname;

	cout << "Enter your name:";
	cin >> myname;

	cout << "Hello, " << myname << endl;
}
