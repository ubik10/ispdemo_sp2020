#include <iostream>

using std::cout;
using std::cin;
using std::endl;


int main()
{
	int x = 100;
	int &bob = x;
	int y = x;

	cout << "x :" << x << endl;
	cout << "bob:" << bob << endl;
	cout << "y :" << y << endl;

	bob = 67;

	cout << "x :" << x << endl;
	cout << "bob:" << bob << endl;
	cout << "y :" << y << endl;

    //&bob = y; // creates an error

	cout << "x :" << x << endl;
    cout << "bob:" << bob << endl;
	cout << "y :" << y << endl;
	

}


