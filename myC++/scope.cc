#include <iostream>

using std::cout;
using std::endl;

int X = 42;

int main()
{

	int z = -1;

	cout << "inside main, local" << endl;
	cout << X << endl;
	cout << z << endl;

	{
		int y = 12;
		int z = 10;
		cout << "inner scope" << endl;

		cout << X << endl;
		cout << y << endl;
		cout << z << endl;


	}

	//what scope is this line in?
    cout << z << endl;


}
