#include <iostream>

using std::cout;
using std::endl;

int hello(int x);

int main()
{
   int local_x = 5;	
   
   hello(local_x);
   
   cout << local_x << endl;

   return 0;
}

int hello(int x)
{
   cout << x << endl;

   for (int i = 0; i < x; i++)
   {
      cout << "hello world!" << endl;
   }

   x = 10;

   return 0;
}
