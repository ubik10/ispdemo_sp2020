#include <iostream>

using std::cin;
using std::cout;
using std::endl;


int main()
{
	int a;

	cin >> a;

	cout << a%2 << endl;

	if (a%2 == 0)
		cout << "a is even" << endl;
	else 
		cout << "a is odd" << endl;

	return 0;
}
