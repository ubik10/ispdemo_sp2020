#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int hello(int &x);

int main()
{
	int local_x = 10;

	hello(local_x);

	cout << local_x << endl;

	return 0;
}

int hello(int &x)
{
	for (int i = 0; i< x; i++)
	{
		cout << "hello world!" << endl;
	}
	x = 100;
	return 0;
}
